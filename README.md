## Surface-Subsurface Coupling Test: Superslab


This is the superslab test reported in Kollet etal., (https://doi.org/10.1002/2016WR019191). It models rainfall-infiltration-runoff on a sloped plane with heterogeneous soil properties. 
